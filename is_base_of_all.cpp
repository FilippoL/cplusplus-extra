#include <iostream>
#include <type_traits>

//// FIRST OPTION: only use c++11
template <class ...empty_args>
class is_base_of_all {
public:
    using type = std::true_type;
    constexpr static bool value = true;
};

template <class base, class first_derived, class ...derived>
class is_base_of_all<base, first_derived, derived...> {
public:
    using type = typename std::conjunction<typename is_base_of_all<base, derived...>::type, 
                                           typename std::is_base_of<base, first_derived>::type
                                           >;
    constexpr static bool value = is_base_of_all<base, derived...>::value && std::is_base_of<base, first_derived>::value;
};

// SECOND OPTION: uses c++17, conjunction and fold expression
template <class base, class ...derived>
class is_base_of_all_2 {
public:
    using type = typename std::conjunction<typename std::is_base_of<base, derived>::type...>;
    constexpr static bool value = (... && std::is_base_of<base, derived>::value);
};

//////////////////////////////
//////////////////////////////
///// TESTING ////////////////
//////////////////////////////
//////////////////////////////

// Classes for testing
class Base {};
class Derived1 : Base {};
class Derived2 : Base {};
class NonDerived1 {};
class NonDerived2 {};
 
// Test is_base_of_all
template<typename T, typename... Ts>
std::enable_if_t<is_base_of_all<T, Ts...>::value>
test_base_of_all(T, Ts...) {
    std::cout << "T is base of all classes..." << std::endl;
}
 
template<typename T, typename... Ts>
std::enable_if_t<!is_base_of_all<T, Ts...>::value>
test_base_of_all(T, Ts...) {
    std::cout << "Not a Base of one or more classes..." << std::endl;
}

// Test is_base_of_all_2
template<typename T, typename... Ts>
std::enable_if_t<is_base_of_all_2<T, Ts...>::value>
test_base_of_all_2(T, Ts...) {
    std::cout << "T is base of all classes..." << std::endl;
}
 
template<typename T, typename... Ts>
std::enable_if_t<!is_base_of_all_2<T, Ts...>::value>
test_base_of_all_2(T, Ts...) {
    std::cout << "Not a Base of one or more classes..." << std::endl;
}

int main() {
    // Test is_base_of_all
    test_base_of_all(Base(), Derived1(), Derived2());
    test_base_of_all(Base(), NonDerived1(), Derived2());
    test_base_of_all(Base(), Derived1(), NonDerived2());
    test_base_of_all(Base(), NonDerived1(), NonDerived2());
    
    // Test is_base_of_all_2
    test_base_of_all_2(Base(), Derived1(), Derived2());
    test_base_of_all_2(Base(), NonDerived1(), Derived2());
    test_base_of_all_2(Base(), Derived1(), NonDerived2());
    test_base_of_all_2(Base(), NonDerived1(), NonDerived2());
}
